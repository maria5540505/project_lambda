#include <stdio.h>
#include <math.h>

void bulldozer(int a, int b, int c);

int main(){
    int a,b,c;
    a = printf("Enter the coefficient (a): ");
    scanf("%d",&a);
    b = printf("Enter the coefficient (b): ");
    scanf("%d",&b);
    c = printf("Enter the coefficient (c): ");
    scanf("%d",&c);
    if (a ==0){
        printf("Error!!!The coefficient of a cannot be zero");
        return 1;
    }bulldozer(a,b,c);
    return 0;
}void bulldozer(int a, int b, int c){
    int disc_;
    disc_ = b*b - 4*a*c;
    if (disc_ == 0){
        double root = -b/(2*a);
        printf("\nReal and Equal\nRoot: %.4f",root);
    }else if (disc_ >= 0){
        double root1 = (-b + sqrt(disc_)/2.0*a);
        double root2 = (-b - sqrt(disc_)/2.0*a);
        printf("\nReal and Distinct\nRoot1: %.4f\nRoot2: %.4f",root1,root2);
    }else{
        printf("\nComplex");
    }
}