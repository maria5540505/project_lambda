#include <stdio.h>
#include <stdlib.h>

struct Node
{
    int number;
    struct Node *next;
};

// Function prototypes
struct Node *createNode(int num){
    struct Node* newNode=(struct Node*)malloc(sizeof(struct Node));
    if (newNode==NULL){
        printf("memory allocation failed!\n");
        exit(1);
    }
    newNode-> number= num;
    newNode-> next= NULL;
    return newNode;
}

void printList(struct Node *head){
if (head == NULL){
        printf("List is empty\n");
        return;
}
    struct Node* current = head;
    int index =0;
    while (current != NULL);{
        printf("Node %d: %d\n", index, current->number);
        current = current->next;
        index++;
    }
}



void append(struct Node **head, int num){
    struct Node*newNode= createNode(num);
    if(*head==NULL){
        *head=newNode;
        return;
    }
    struct Node* temp=*head;
    while (temp->next !=NULL){
        temp = temp-> next;  
    }
    temp ->next=newNode;
} 


void prepend(struct Node **head, int num){
    struct Node* newNode= createNode(num);
    newNode-> next=*head;
    *head=newNode;
}
    

void deleteByKey(struct Node **head, int key){
    struct Node* temp=*head;
    struct Node* prev=NULL;
    if(temp!= NULL&& temp-> number== key){
        *head = temp->next;
        free(temp);
        return;
    }
    if (temp!= NULL && temp->number !=key){
        *head = temp->next;
        free(temp);
        return;
    }
    while (temp != NULL && temp->number !=key){
        prev = temp;
        temp = temp-> next;
    }
    if (temp==NULL) return;
    prev->next= temp->next;
    free(temp);
}  


void deleteByValue(struct Node **head, int value){
    struct Node* current=*head;
    struct Node* prev=NULL;
    if(current!= NULL && current-> number== value){
        *head = current->next;
        free(current);
        return;
    }
    if (current!= NULL && current->number !=value){
        *head = current->next;
        free(current);
        return;
    }
    while (current != NULL && current->number !=value){
        prev = current;
        current = current-> next;
    }
    if (current == NULL) return;
    prev -> next= current -> next;
    free(current);

}

void insertAfterKey(struct Node **head, int key, int value){
    struct Node* current = *head;
    while (current != NULL && current ->number !=key){
        current= current -> next;
    }
    if ( current == NULL){
        printf( "key %d not found in the lunked list.\n",key);
        return;
    }
    struct Node* new_node = (struct Node*)malloc(sizeof(struct Node));
    new_node->next = current-> next;
    current-> next = new_node;
}


void insertAfterValue(struct Node **head, int searchValue, int newValue){
    struct Node* current = *head;
    while (current != NULL && current ->number != searchValue){
        current= current -> next;
    }
    if ( current == NULL){
        printf( "value %d not found in the lunked list.\n",searchValue);
        return;
    }
    struct Node* new_node = (struct Node*)malloc(sizeof(struct Node));
    new_node->next = current-> next;
    current-> next = new_node;
}


int main()
{
    struct Node *head = NULL;
    int choice, data;

    while (1)
    {
       printf("Linked Lists\n");
        printf("1. Print List\n");
        printf("2. Append\n");
        printf("3. Prepend\n");
        printf("4. Delete\n");
        printf("5. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        /*if (choice == 1)
        {
            printList(head);
            break;
        }
        else if (choice == 2)
        {
            printf("Enter data to append: ");
            scanf("%d", &data);
            append(&head, data);
            break;
        }
        else
        {
            printf("Invalid choice. Please try again.\n");
        }*/

        switch (choice){
            case 3:
            printf("enter data to prepend: ");
            scanf("%d",&data);
            break;
            case 4:
            printf("enter data to delete: ");
            scanf("%d",&data);
            break;
            case 5:
            printf(" enter data to exit: ");
            scanf("%d",&data);
            break;
            default:
            printf("you have entered a wrong choice");

        }
    }

    return 0;
}

// Questions:
// Implement the prototypes defined above.
// Reimplement the main method using a switch and complete the pending steps.